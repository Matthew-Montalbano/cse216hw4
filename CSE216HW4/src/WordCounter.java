import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;


public class WordCounter {

    // The following are the ONLY variables we will modify for grading.
    // The rest of your code must run with no changes.
    public static final Path FOLDER_OF_TEXT_FILES  = Paths.get(""); // path to the folder where input text files are located
    public static final Path WORD_COUNT_TABLE_FILE = Paths.get(""); // path to the output plain-text (.txt) file
    public static final int  NUMBER_OF_THREADS     = 1;                // max. number of threads to spawn



    public static void main(String... args) {
        try {
            List<Path> paths = Files.walk(FOLDER_OF_TEXT_FILES)
                                                .filter(Files::isRegularFile)
                                                .sorted()
                                                .collect(Collectors.toList());
            if (paths.size() == 0) {
                System.out.println("There are no files in this directory.");
                return;
            }
            long start = System.currentTimeMillis();
            ArrayList<HashMap<String, Integer>> wordMapsList = countWordsFromAllFiles(paths);
            long end = System.currentTimeMillis();
            System.out.printf("%6d ms%n", end - start);
            ArrayList<String> fileNames = getFileNames(paths);
            String outputTable = generateOutputTable(fileNames, wordMapsList);
            writeToFile(outputTable);
        } catch (IOException e) {
            System.out.println("Could not walk through directory.");
            System.exit(0);
        }
    }

    private static ArrayList<HashMap<String, Integer>> countWordsFromAllFiles(List<Path> paths) {
        ArrayList<HashMap<String, Integer>> wordMaps = new ArrayList<>();
        addPlaceholders(wordMaps, paths.size());
        ArrayList<Thread> threads = new ArrayList<>();
        int threadsLeft = NUMBER_OF_THREADS;
        for (int i = 0; i < paths.size(); i++) {
            if (threadsLeft <= 1) {
                HashMap<String, Integer> wordMap = countWordsInFile(paths.get(i).toFile());
                wordMaps.set(i, wordMap);
            } else {
                FileThread thread = new FileThread(paths.get(i), i, wordMaps);
                thread.start();
                threads.add(thread);
                threadsLeft--;
            }
        }
        joinThreads(threads);
        return wordMaps;
    }

    private static void addPlaceholders(List<HashMap<String, Integer>> list, int size) {
        for (int i = 0; i < size; i++) {
            list.add(new HashMap<>());
        }
    }

    private static class FileThread extends Thread {
        private Path path;
        private int mapLocation;
        private ArrayList<HashMap<String, Integer>> wordMaps;

        public FileThread(Path path, int mapLocation, ArrayList<HashMap<String, Integer>> maps) {
            this.path = path;
            this.mapLocation = mapLocation;
            this.wordMaps = maps;
        }

        public void run() {
            HashMap<String, Integer> wordMap = countWordsInFile(path.toFile());
            wordMaps.set(mapLocation, wordMap);
        }
    }

    private static HashMap<String, Integer> countWordsInFile(File file) {
        HashMap<String, Integer> wordMap = new HashMap<>();
        try {
            Reader reader = new BufferedReader(new FileReader(file));
            wordMap = countWordsInFileHelper(reader);
        } catch (IOException e) {
            System.out.println("Could not read file: " + file.toString());
            System.exit(0);
        }
        return wordMap;
    }

    private static HashMap<String, Integer> countWordsInFileHelper(Reader reader) throws IOException {
        char currentChar;
        StringBuilder currentWord = new StringBuilder();
        HashMap<String, Integer> wordMap = new HashMap<>();
        int data = reader.read();
        while (data != -1) {
            if (isLetter(data) || data == 39) {
                if (data != 39) {
                    currentChar = (char) data;
                    currentWord.append(currentChar);
                }
            } else if (currentWord.length() > 0) {
                addWordToMap(currentWord, wordMap);
                currentWord.delete(0, currentWord.length());
            }
            data = reader.read();
        }
        if (currentWord.length() > 0) {
            addWordToMap(currentWord, wordMap);
        }
        return wordMap;
    }

    private static boolean isLetter(int charCode) {
        return (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122);
    }

    private static void addWordToMap(StringBuilder builder, HashMap<String, Integer> map) {
        String word = builder.toString().toLowerCase();
        map.put(word, map.getOrDefault(word, 0) + 1);
    }

    private static void joinThreads(ArrayList<Thread> threads) {
        for (Thread thread: threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.println(thread + " was interrupted");
            }
        }
    }

    private static ArrayList<String> getFileNames(List<Path> paths) {
        ArrayList<String> names = new ArrayList<>();
        for (Path path: paths) {
            names.add(getFileNameWithoutExtension(path));
        }
        return names;
    }

    private static String getFileNameWithoutExtension(Path path) {
        String fileName = path.getFileName().toString();
        return fileName.substring(0, fileName.length() - 4);
    }

    public static String generateOutputTable(ArrayList<String> fileNames, ArrayList<HashMap<String, Integer>> wordMapsList) {
        StringBuilder table = new StringBuilder();
        ArrayList<String> sortedWords = getListOfWordsSorted(wordMapsList);
        int wordColumnSize = getLongestWordLength(sortedWords) + 1;
        int fileColumnSize = getLongestWordLength(fileNames) + 3;
        table.append(buildFirstRowString(fileNames, wordColumnSize, fileColumnSize));
        for (String word: sortedWords) {
            table.append(buildRowString(wordMapsList, word, wordColumnSize, fileColumnSize));
        }
        return table.toString();
    }

    private static ArrayList<String> getListOfWordsSorted(ArrayList<HashMap<String, Integer>> wordMapsList) {
        Set<String> setOfWords = getSetOfWords(wordMapsList);
        ArrayList<String> listOfWords = new ArrayList<>(setOfWords);
        Collections.sort(listOfWords);
        return listOfWords;
    }

    private static Set<String> getSetOfWords(ArrayList<HashMap<String, Integer>> list) {
        Set<String> allWords = new HashSet<>();
        for (HashMap<String, Integer> wordMap: list) {
            allWords.addAll(wordMap.keySet());
        }
        return allWords;
    }

    private static <T extends Collection<String>> int getLongestWordLength(T allWords) {
        int longest = 0;
        for (String word: allWords) {
            longest = Math.max(word.length(), longest);
        }
        return longest;
    }

    private static String buildFirstRowString(ArrayList<String> fileNames, int wordColumnWidth, int fileColumnWidth) {
        StringBuilder rowString = new StringBuilder();
        rowString.append(spaceString(wordColumnWidth));
        for (String fileName: fileNames) {
            rowString.append(fileName);
            rowString.append(spaceString(fileColumnWidth - fileName.length()));
        }
        rowString.append("total \n");
        return rowString.toString();
    }

    private static String buildRowString(ArrayList<HashMap<String, Integer>> wordMapsList, String word, int wordColumnWidth, int fileColumnWidth) {
        StringBuilder rowString = new StringBuilder();
        int totalWordOccurrences = 0;
        int wordOccurrences;
        rowString.append(word);
        rowString.append(spaceString(wordColumnWidth - word.length()));
        for (HashMap<String, Integer> wordMap: wordMapsList) {
            wordOccurrences = wordMap.getOrDefault(word, 0);
            rowString.append(wordOccurrences);
            rowString.append(spaceString(fileColumnWidth - String.valueOf(wordOccurrences).length()));
            totalWordOccurrences += wordOccurrences;
        }
        rowString.append(totalWordOccurrences);
        rowString.append("\n");
        return rowString.toString();
    }

    private static String spaceString(int length) {
        StringBuilder spaces = new StringBuilder();
        for (int i = 0; i < length; i++) {
            spaces.append(" ");
        }
        return spaces.toString();
    }

    private static void writeToFile(String output) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(WORD_COUNT_TABLE_FILE.toString() + "\\output.txt"));
            String[] rows = output.split("\n");
            for (String row: rows) {
                writer.write(row);
                writer.newLine();
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Could not write to file.");
            System.exit(0);
        }
    }
}