import java.util.Random;
import java.util.stream.IntStream;
import java.lang.Thread;

public class MergeSort {

    private static final Random RNG    = new Random(10982755L);
    private static final int    LENGTH = 524288; //524288
    private static final int    NUMBER_OF_THREADS = 1;

    public static void main(String... args) {
        int[] arr = randomIntArray();
        long start = System.currentTimeMillis();
        concurrentMergeSort(arr);
        long end = System.currentTimeMillis();
        if (!sorted(arr)) {
            System.err.println("The final array is not sorted");
            System.exit(0);
        }
        System.out.printf("%10d numbers: %6d ms%n", LENGTH, end - start);
    }

    private static int[] randomIntArray() {
        int[] arr = new int[LENGTH];
        for (int i = 0; i < arr.length; i++)
            arr[i] = RNG.nextInt(LENGTH * 10);
        return arr;
    }

    public static boolean sorted(int[] arr) {
        return !IntStream.range(1, arr.length)
                .mapToObj(i -> arr[i - 1] > arr[i])
                .filter(i -> i == true)
                .findFirst().orElse(false);
    }

    public static void concurrentMergeSort(int[] array) {
        concurrentMergeSort(array, NUMBER_OF_THREADS);
    }

    public static void concurrentMergeSort(int[] array, int threadCount) {
        if (array.length == 1) {
            return;
        }

        int mid = array.length / 2;
        int[] leftArray = new int[mid];
        int[] rightArray = new int[array.length - mid];

        for (int i = 0; i < mid; i++) {
            leftArray[i] = array[i];
        }

        int rightIndex = 0;
        for (int i = mid; i < array.length; i++) {
            rightArray[rightIndex++] = array[i];
        }

        if (threadCount == 1) {
            concurrentMergeSort(leftArray, 1);
            concurrentMergeSort(rightArray, 1);
        } else {
            mergeSortOnThreads(leftArray, rightArray, threadCount / 2);
        }
        merge(array, leftArray, rightArray);
    }

    private static void mergeSortOnThreads(int[] leftArray, int[] rightArray, int threadCount) {
        Thread leftThread = new Thread(new Sorting(leftArray, threadCount));
        Thread rightThread = new Thread(new Sorting(rightArray, threadCount));
        leftThread.start();
        rightThread.start();
        try {
            leftThread.join();
            rightThread.join();
        } catch (InterruptedException e) {
            System.out.println("Thread failed to complete");
        }
    }

    private static void merge(int[] fullArray, int[] leftArray, int[] rightArray) {
        int leftIndex = 0;
        int rightIndex = 0;
        int mainIndex = 0;
        int leftValue, rightValue;
        while (leftIndex < leftArray.length && rightIndex < rightArray.length) {
            leftValue = leftArray[leftIndex];
            rightValue = rightArray[rightIndex];
            if (leftValue <= rightValue) {
                fullArray[mainIndex] = leftValue;
                leftIndex++;
            } else {
                fullArray[mainIndex] = rightValue;
                rightIndex++;
            }
            mainIndex++;
        }

        if (leftIndex >= leftArray.length) {
            copyRemainingValues(fullArray, rightArray, mainIndex, rightIndex);
        } else {
            copyRemainingValues(fullArray, leftArray, mainIndex, leftIndex);
        }
    }

    private static void copyRemainingValues(int[] destArray, int[] sourceArray, int destIndex, int sourceIndex) {
        while (sourceIndex < sourceArray.length) {
            destArray[destIndex++] = sourceArray[sourceIndex++];
        }
    }
}