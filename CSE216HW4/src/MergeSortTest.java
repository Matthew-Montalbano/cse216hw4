import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MergeSortTest {
    @Test
    public void testSorted() {
        int[] emptyArray = {};
        int[] arrayOne = {1, 2, 3};
        int[] arrayTwo = {1, 3, 2};
        int[] arrayThree = {0};
        int[] arrayFour = {-3, -2, -1};
        int[] arrayFive = {-1, -2, -3};
        int[] arraySix = {-1, 0, 1};
        int[] arraySeven = {1, 0, -1};
        int[] arrayEight = {Integer.MIN_VALUE, 1};
        int[] arrayNine = {1, Integer.MIN_VALUE};
        int[] arrayTen = {1, Integer.MAX_VALUE};
        int[] arrayEleven = {Integer.MAX_VALUE, 1};
        int[] arrayTwelve = {0, 0, 0, 1};
        assertAll(() -> assertTrue(MergeSort.sorted(emptyArray)),
                () -> assertTrue(MergeSort.sorted(arrayOne)),
                () -> assertFalse(MergeSort.sorted(arrayTwo)),
                () -> assertTrue(MergeSort.sorted(arrayThree)),
                () -> assertTrue(MergeSort.sorted(arrayFour)),
                () -> assertFalse(MergeSort.sorted(arrayFive)),
                () -> assertTrue(MergeSort.sorted(arraySix)),
                () -> assertFalse(MergeSort.sorted(arraySeven)),
                () -> assertTrue(MergeSort.sorted(arrayEight)),
                () -> assertFalse(MergeSort.sorted(arrayNine)),
                () -> assertTrue(MergeSort.sorted(arrayTen)),
                () -> assertFalse(MergeSort.sorted(arrayEleven)),
                () -> assertTrue(MergeSort.sorted(arrayTwelve))
        );
    }
}
